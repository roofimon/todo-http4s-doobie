package service

import cats.effect.IO
import fs2.Stream
import io.circe.generic.auto._
import io.circe.syntax._
import io.circe.{Decoder, Encoder}
import model.{Importance, Todo, TodoNotFoundError}
import org.http4s.circe._
import org.http4s.dsl.Http4sDsl
import org.http4s.headers.{Location, `Content-Type`}
import org.http4s.{HttpRoutes, MediaType, Uri}
import repository.TodoRepository
import org.http4s.Response

class TodoService(repository: TodoRepository) extends Http4sDsl[IO] {
  private implicit val encodeImportance: Encoder[Importance] =
    Encoder.encodeString.contramap[Importance](_.value)

  private implicit val decodeImportance: Decoder[Importance] =
    Decoder.decodeString.map[Importance](Importance.unsafeFromString)

  val routes = HttpRoutes.of[IO] {
    case GET -> Root / "todos" =>
      Ok(
        Stream("[") ++ repository.getTodos
          .map(_.asJson.noSpaces)
          .intersperse(",") ++ Stream("]"),
        `Content-Type`(MediaType.application.json)
      )

    case GET -> Root / "todos" / LongVar(id) =>
      repository.getTodo(id).flatMap(getResult => todoResult(getResult))

    case req @ POST -> Root / "todos" =>
      req
        .decodeJson[Todo]
        .flatMap(todo => repository.createTodo(todo))
        .flatMap(newTodo =>
          Created(
            newTodo.asJson,
            Location(Uri.unsafeFromString(s"/todos/${newTodo.id.get}"))
          )
        )

    case req @ PUT -> Root / "todos" / LongVar(id) =>
      for {
        todo <- req.decodeJson[Todo]
        updateResult <- repository.updateTodo(id, todo)
        response <- todoResult(updateResult)
      } yield response

    case DELETE -> Root / "todos" / LongVar(id) =>
      repository.deleteTodo(id).flatMap {
        case Left(TodoNotFoundError) => NotFound()
        case Right(_)                => NoContent()
      }
  }

  private def todoResult(
      result: Either[TodoNotFoundError.type, Todo]
  ): IO[Response[IO]] = {
    result match {
      case Left(TodoNotFoundError) => NotFound()
      case Right(todo)             => Ok(todo.asJson)
    }
  }
}
